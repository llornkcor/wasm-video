#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMediaPlayer>
#include <QGraphicsVideoItem>
#include <QVideoWidget>
#include <QAudioOutput>

#include <QGraphicsScene>

#include <QtWidgets>
#include <QVideoSink>
#include <QVideoWidget>

// #define USE_VIDEOWIDGET

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // https://test-videos.co.uk/bigbuckbunny/mp4-h264
//    ui->lineEdit->setText("https://127.0.0.1:8001/big_buck_bunny_720p_30mb.mp4");

    //ui->lineEdit->setText("https://192.168.1.137/media/big_buck_bunny_720p_30mb.mp4");

    ui->lineEdit->setText("https://192.168.1.137/wasm/Big_Buck_Bunny_4k.mp4");

    //  ui->lineEdit->setText("https://127.0.0.1:8001/Big_Buck_Bunny_720_10s_5MB.mp4");
/*
 * /media/Big_Buck_Bunny_1080_10s_5MB.mp4
 * /media/Big_Buck_Bunny_360_10s_30MB.mp4
 * /media/Big_Buck_Bunny_720_10s_5MB.mp4
 * /media/big_buck_bunny_720p_30mb.mp4
 *
 * https://test-videos.co.uk/vids/bigbuckbunny/mp4/h265/720/Big_Buck_Bunny_720_10s_5MB.mp4
 * https://test-videos.co.uk/vids/bigbuckbunny/mp4/av1/720/Big_Buck_Bunny_720_10s_5MB.mp4
 * https://test-videos.co.uk/vids/bigbuckbunny/mp4/av1/720/Big_Buck_Bunny_720_10s_5MB.mp4
 * https://test-videos.co.uk/vids/bigbuckbunny/webm/vp8/720/Big_Buck_Bunny_720_10s_5MB.webm
 * https://test-videos.co.uk/vids/bigbuckbunny/webm/av1/720/Big_Buck_Bunny_720_10s_5MB.webm
 * https://test-videos.co.uk/vids/bigbuckbunny/mkv/720/Big_Buck_Bunny_720_10s_5MB.mkv
 *
 * https://192.168.1.137/media/Big_Buck_Bunny_720_10s_5MB.mp4
 * https://192.168.1.137/media/Big_Buck_Bunny_720_10s_5MB.mp4
 * https://192.168.1.137/media/Big_Buck_Bunny_720_10s_5MB.mp4
 * https://192.168.1.137/media/Big_Buck_Bunny_720_10s_5MB.webm
 * https://192.168.1.137/media/Big_Buck_Bunny_720_10s_5MB.webm
 * https://192.168.1.137/media/Big_Buck_Bunny_720_10s_5MB.mkv
 * */
    playIt();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::playIt()
{
    player = new QMediaPlayer(this);
    player->setAudioOutput(&audioOutput);

#ifdef USE_VIDEOWIDGET
    QVideoWidget *videoOutput = new QVideoWidget(this);
    player->setVideoOutput(videoOutput);
    //videoOutput->resize(640, 360);
#else
    item = new QGraphicsVideoItem();



    QGraphicsScene *scene = new QGraphicsScene(this);
    player->setVideoOutput(item);

    ui->graphicsView->setScene(scene);
    ui->graphicsView->scene()->addItem(item);
    ui->graphicsView->show();

    qWarning() << "<<<<<<<<< Video frame size " << item->size();
#endif

    connect(player, &QMediaPlayer::positionChanged, this, [=] (qint64 position){
        qWarning() << "position changed" << position;
        ui->positionLabel->setText(QString::number(position));
        ui->progressSlider->setValue(position);

       // player->duration
    });
    connect(player, &QMediaPlayer::errorChanged, this, [=] (){
        if (player->error() == QMediaPlayer::NoError)
            return;
        qWarning() << "errorChanged" << player->errorString();
        //  ui->textEdit->setText(player->errorString());
    });

    connect(player, &QMediaPlayer::mediaStatusChanged, this, &MainWindow::statusChanged);

    connect(player, &QMediaPlayer::playbackStateChanged, this,
            [=] (QMediaPlayer::PlaybackState newState){
        qWarning() << "playbackStateChanged" << newState;
        //    ui->textEdit->setText("new state");
    });

    connect(player, &QMediaPlayer::durationChanged, this,
            [=] (qint64 duration){
        qWarning() << "duration changed" << duration;
        ui->durationValueLabel->setText(QString::number(duration));
        ui->progressSlider->setMaximum(duration);
    });

    connect(player, &QMediaPlayer::bufferProgressChanged, this,
            [=] (float filled){
        qWarning() << "buffer progress" << filled;
        ui->progressBar->setValue(filled * 100);
    });

    connect(player, &QMediaPlayer::metaDataChanged, this,
            [=] (){
        qWarning() << "metadata changed";
    });


    connect(player->videoSink(), &QVideoSink::videoSizeChanged, this,
            [=] (){
        qWarning() << "videoSizeChanged"
                   << player->videoSink()->videoSize();
        // item->setSize(player->videoSink()->videoSize());
    });

    ui->volumeSlider->setValue(audioOutput.volume() * 100);

}

void MainWindow::on_pushButton_clicked()
{
    qWarning() << Q_FUNC_INFO;

    if (urlStr.isEmpty())
        urlStr = ui->lineEdit->text();

    qWarning() << Q_FUNC_INFO << urlStr;

    if (player->source() != QUrl(urlStr))
        player->setSource(QUrl(urlStr));
    else
        player->play();
}

void MainWindow::on_pauseButton_clicked()
{
    player->pause();
}

void MainWindow::on_stopButton_clicked()
{
    player->stop();
}

void MainWindow::on_rewindButton_clicked()
{
    qWarning() << player->position();
}

void MainWindow::on_fastforwardButton_clicked()
{
    qWarning() << player->position();
}

void MainWindow::statusChanged(QMediaPlayer::MediaStatus status)
{
    // handle status message
    switch (status) {
    case QMediaPlayer::NoMedia:
        setStatusInfo("No Media");
        break;
    case QMediaPlayer::LoadedMedia:
    {
        setStatusInfo("LoadedMedia");
        if (!player->isSeekable())
            ui->progressSlider->setFocusPolicy(Qt::NoFocus);
        else
            ui->progressSlider->setFocusPolicy(Qt::TabFocus);
        qreal rateValue = player->playbackRate();
        ui->playbackRateDial->setValue(rateValue * 2);
        ui->playbackRateLabel->setText(QString::number((double)rateValue,'f', 1));

        player->play();
    }
        break;
    case QMediaPlayer::LoadingMedia:
        setStatusInfo(tr("Loading..."));
        break;
    case QMediaPlayer::BufferingMedia:
        setStatusInfo(tr("Buffering..."));
        break;
    case QMediaPlayer::BufferedMedia:
        setStatusInfo(tr("<<<<<<<<<<<<<<<<<< Buffered %1%").arg(qRound(player->bufferProgress()*100.)));
        break;
    case QMediaPlayer::StalledMedia:
        setStatusInfo(tr("Stalled %1%").arg(qRound(player->bufferProgress()*100.)));
        break;
    case QMediaPlayer::EndOfMedia:
        //        QApplication::alert(this);
        //        m_playlist->next();
        setStatusInfo("End of Media");
        break;
    case QMediaPlayer::InvalidMedia:
        setStatusInfo("InvalidMedia");
        break;
    }
}

void MainWindow::setStatusInfo(const QString &info)
{
    qWarning() << Q_FUNC_INFO << info;
}

void MainWindow::on_volumeSlider_valueChanged(int value)
{
    float vol = (float)value / 100;
    qWarning() << Q_FUNC_INFO << value <<  vol << player->audioOutput();
 //   QAudioOutput *audio =  player->audioOutput();
    //if (audioOutput != nullptr) {
        audioOutput.setVolume(vol);
        ui->volumeLabel->setText(QString::number(value));
  //  } else
 //       qWarning() << "QAudioOutput is NULL";
}

void MainWindow::on_progressSlider_sliderReleased()
{
     qWarning() << Q_FUNC_INFO << player->isSeekable();
    if (!player->isSeekable()) {
        player->play();
        return;
    }
    qint64 pos = ui->progressSlider->value();
    qWarning() << Q_FUNC_INFO << pos;
    player->setPosition(pos);
    player->play();
}

void MainWindow::on_progressSlider_sliderPressed()
{
    player->pause();
}

void MainWindow::on_playbackRateDial_valueChanged(int value)
{
    if (value == 0)
        value = 1;
    qWarning() << Q_FUNC_INFO << value;
    player->setPlaybackRate((float)value / 2);
    ui->playbackRateLabel->setText(QString::number((float)value / 2));
}

void MainWindow::on_resolutionComboBox_activated(int index)
{
/* 720p
  1080p
  4k */
    qWarning() << index;
    QSize newSize;
    switch (index) {
    case 0: // 720
        newSize = QSize(1280,720);
        break;
    case 1: // 1080
        newSize = QSize(1920,1080);
        break;
    case 2://  4k
        newSize = QSize(3840,2160);
        break;
    };
  //   player->videoSink()
     item->setSize(newSize);
}


void MainWindow::on_urlComboBox_textActivated(const QString &arg1)
{
    qWarning() << arg1;
    urlStr = arg1;
    ui->lineEdit->setText(urlStr);
}


void MainWindow::on_lineEdit_editingFinished()
{
    urlStr = ui->lineEdit->text();
    qWarning() << Q_FUNC_INFO << urlStr;
    ui->lineEdit->setText(urlStr);
}

