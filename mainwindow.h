#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QGraphicsVideoItem>
#include <QAudioOutput>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void statusChanged(QMediaPlayer::MediaStatus status);
    void on_pushButton_clicked();

    void on_pauseButton_clicked();

    void on_stopButton_clicked();

    void on_rewindButton_clicked();

    void on_fastforwardButton_clicked();

    void on_volumeSlider_valueChanged(int value);

    void on_progressSlider_sliderReleased();

    void on_progressSlider_sliderPressed();

    void on_playbackRateDial_valueChanged(int value);

    void on_resolutionComboBox_activated(int index);

    void on_urlComboBox_textActivated(const QString &arg1);

    void on_lineEdit_editingFinished();

private:
    Ui::MainWindow *ui;
    QMediaPlayer *player;
    void playIt();

    QGraphicsVideoItem *item;
    QAudioOutput audioOutput; // chooses the default audio routing
    QString urlStr;

    void setStatusInfo(const QString &info);

};
#endif // MAINWINDOW_H
